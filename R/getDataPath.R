#' Give the complete path of the data
#'
#' If the database is a URL, this function can download the file in a temporary folder and returns the path of the downloaded file. This functionality is useful for [readxl::read_excel] (See: https://stackoverflow.com/a/41368947/5300212).
#'
#' @param path [character] representing the path of the data in the database
#' @param download download a URL in a temporary file? (Default `FALSE`)
#' @param cfg configuration which contains the location of the database (See [getConfig])
#'
#' @return [character] with the path of the file to read
#' @export
#'
#' @examples
#' com_france <- readxl::read_excel(
#'   getDataPath("usages/territoire/table-appartenance-geo-communes-16.xls", download = TRUE),
#'   sheet = "COM",
#'   skip = 5
#' )
#' str(com_france)
getDataPath <- function(path, download = FALSE, cfg = getConfig()) {

  if(cfg$data$onCloud) {
    file <- basename(path)
    ext <- tools::file_ext(file)
    folder <- utils::URLencode(dirname(path), reserved = TRUE)
    url1 <- paste0(cfg$data$path, folder, "&files=", utils::URLencode(file, reserved = TRUE))
    if(download) {
      httr::GET(url1, httr::write_disk(path <- paste(tempfile(), ext, sep = ".")))
      return(path)
    } else {
      return(url1)
    }
  } else {
    return(paste0(cfg$data$path, path))
  }
}
