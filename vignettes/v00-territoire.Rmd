---
title: "Analyse des données de territoire du bassin versant"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Analyse des données de territoire du bassin versant}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  echo = FALSE,
  collapse = TRUE,
  comment = "#>", 
  fig.width = 6
)
```

```{r setup, warning = FALSE, message = FALSE}
library(InWopUsages)
territoire <- get_territoire(comsurfmin = 0)
```

Le Bassin versant comprend `r nrow(territoire$com)` communes situées dans `r nrow(territoire$arr)` arrondissements eux-mêmes situés dans `r nrow(territoire$dep)` départements.

## Suppression des communes tronquées

`r length(which(territoire$com$surf_bv / territoire$com$surf_tot < 0.99))` communes sont tronquées par le découpage en bassin versant. La distribution de ces communes en fonction de leur part de surface dans le bassin versant est la suivante :

```{r}
probs <- seq(0, 0.99, 0.01)
dfComTrunc <- territoire$com %>% filter(surf_bv / surf_tot < 0.99) 
dfComTruncQuant <- data.frame(nb_com = probs * length(which(territoire$com$surf_bv / territoire$com$surf_tot < 0.99)), 
                              prop_surf = quantile(dfComTrunc$prop_surf, probs = probs))
library(ggplot2)
ggplot(dfComTruncQuant, aes(x = prop_surf, y = nb_com)) +
  geom_line() +
  scale_x_continuous(breaks = seq(0, 100, 10)) +
  labs(
       x = "Part de la surface de la commune sur le BV (%)", 
       y = "Nombre de communes")
```

```{r}
dfComTruncQuant$surf_cum <- sapply(dfComTruncQuant$prop_surf, function(prop_surf) {sum(dfComTrunc$surf_bv[dfComTrunc$prop_surf <= prop_surf])})
ggplot(dfComTruncQuant, aes(x = prop_surf, y = surf_cum)) +
  geom_line() +
  scale_x_continuous(breaks = seq(0, 100, 10)) + 
  labs(
     x = "Part de la surface de la commune sur le BV (%)", 
     y = "Surface cumulée du bassin versant km²")

```

Les communes ayant une trop petite surface dans le bassin versant peuvent être écartées de l'étude. Un seuil minimum de 25 %  de la surface dans le bassin versant entraîne la suppression de `r length(which(dfComTrunc$prop_surf < 25))` communes pour une surface totale de `r sum(dfComTrunc$surf_bv[dfComTrunc$prop_surf < 25])` km², soit `r round(sum(dfComTrunc$surf_bv[dfComTrunc$prop_surf < 25]) / sum(territoire$com$surf_bv) * 100, 1)` % de la surface total du bassin versant.

```{r}
territoire25 <- get_territoire(comsurfmin = 25)
```


## Découpage en arrondissements

La suppression des communes partiellement présentes sur le bassin 

```{r}
display_tableau <- function(df) {
  knitr::kable(df)
}
display_tableau(territoire25$arr)
```

## Découpage en départements

```{r}
display_tableau(territoire25$dep)
```

