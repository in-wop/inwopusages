---
title: "Localisation des prélèvements et restitutions des canaux"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Localisation des prélèvements et restitutions des canaux}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>", 
  fig.width = 7
)
```

```{r setup}
library(InWopUsages)
dfChroNet <- readRDS("v10-dfChroNet.rds")
dfC <- dfChroNet %>% filter(code_usage == "CAN")

# Nettoyage du nom des ouvrages
dfC <- dfC %>% 
  mutate(nom_ouvrage = iconv(nom_ouvrage, from = "UTF-8", to = "ASCII//TRANSLIT")) %>% 
  mutate(nom_ouvrage = gsub("D\\?rivation", "Dérivation", nom_ouvrage)) %>%
  mutate(nom_ouvrage = gsub("D\\?rivatrion", "Dérivation", nom_ouvrage)) %>%
  mutate(nom_ouvrage = gsub("lat\\?ral", "latéral", nom_ouvrage)) %>%
  mutate(nom_ouvrage = gsub(" \\? ", " à ", nom_ouvrage)) %>%
  mutate(nom_ouvrage = gsub("Berni\\?res", "Bernières", nom_ouvrage)) %>%
  mutate(nom_ouvrage = gsub("\\?", "é", nom_ouvrage))

# Ajout d'une colonne d'identification du canal
dfC <- dfC %>% mutate(canal = trimws(gsub(" - .*", "", nom_ouvrage)))

# Ajout d'une colonne de transfert interne :
# (TRUE pour transfert d'un canal à l'autre = prélèvement à ignorer dans le modèle)
dfC$interne <- FALSE

# Prélèvements minimum, moyen et maximum
dfCM <- dfC %>% group_by(canal, code_ouvrage, nom_ouvrage, code_commune_insee, nom_commune, uri_ouvrage, latitude, longitude) %>%
  summarise(vol_moy = round(mean(volume) / 1E6, 3), 
            vol_min = round(min(volume) / 1E6, 3),
            vol_max = round(max(volume) / 1E6, 3),
            .groups = "drop")
```

# Classement des prises par canal

```{r}
knitr::kable(dfCM %>% 
               group_by(canal) %>% 
               summarise(nb_ouvrages = n(), vol_moy = sum(vol_moy), vol_min = sum(vol_min), vol_max = sum(vol_max)) %>%
               arrange(-vol_moy))
```


Les canaux représentent `r nrow(dfCM)` ouvrages de prélèvement sur le bassin pour un total de `r sum(dfCM$vol_moy)` millions de m<sup>3</sup>/an.

```{r}
library(sf)
sfC <- st_as_sf(dfCM, coords = c("longitude", "latitude"), crs = 4326)
```

```{r map1, fig.asp = .8}
library(tmap)

tmBG <- tm_shape(sfBV) + tm_polygons(border.col = "black", col = "#556655")

tmC <- tmBG + 
  tm_shape(sfC) +
  tm_dots(col = "canal", size = "vol_moy", scale = 2) + 
  tm_scale_bar(text.size = 0.5) +
  tm_layout(legend.outside = TRUE) +
  tm_grid(projection = 4326)

tmC
```

Beaucoup de canaux ne sont pas identifiés (nom d'ouvrage "VOIES NAVIGABLES DE FRANCE), cependant ils sont situés sur des zones précises où il est possible de les réattribuer par zone à partir de leurs coordonnées géographiques.

```{r}
lZones <- list(
  "Canal de l'Aisne à la Marne" = c(xmin = 4.2, xmax = 4.5, ymin = 48.9, ymax = 49.2),
  "Canal latéral Marne" = c(xmin = 4.2, xmax = 4.7, ymin = 48.5, ymax = 49.0),
  "Canal des Ardennes" = c(xmin = 4, xmax = 4.8, ymin = 49.4, ymax = 49.6), 
  "Canal de l'Oise à l'Aisne" = c(xmin = 3.2, xmax = 3.4, ymin = 49.4, ymax = 49.6),
  "Canal de la Sambre à l'Oise" = c(xmin = 3.5, xmax = 3.7, ymin = 49.8, ymax = 50),
  "Canal du Loing" = c(xmin = 2.5, xmax = 3, ymin = 47.5, ymax = 48.5),
  "Canal du Nivernais" = c(xmin = 3.5, xmax = 3.8, ymin = 47.2, ymax = 47.8)
)

for(zone in names(lZones)) {
  sfCrop <- st_crop(sfC, lZones[[zone]]) %>% filter(nom_ouvrage == "VOIES NAVIGABLES DE FRANCE")
  message("Sélection ", zone, ": ", paste(sfCrop$code_ouvrage, collapse = ", "))
  for(code_canal in sfCrop$code_ouvrage) {
    dfC <- dfC %>% mutate(canal = replace(canal, code_ouvrage == code_canal, zone))
  }
}

# Prélèvements minimum, moyen et maximum
dfCM <- dfC %>% group_by(canal, code_ouvrage, nom_ouvrage, code_commune_insee, nom_commune, uri_ouvrage, latitude, longitude) %>%
  summarise(vol_moy = round(mean(volume) / 1E6, 3), 
            vol_min = round(min(volume) / 1E6, 3),
            vol_max = round(max(volume) / 1E6, 3),
            .groups = "drop")

sfC <- st_as_sf(dfCM, coords = c("longitude", "latitude"), crs = 4326)
```

```{r, eval = FALSE, echo = FALSE}
knitr::kable(
  sfC %>% filter(nom_ouvrage == "VOIES NAVIGABLES DE FRANCE") %>% 
    select(code_ouvrage, canal, nom_commune, vol_min, vol_moy, vol_max, -geometry) %>% 
    arrange(canal, code_ouvrage)
)
```

```{r map2, fig.asp = .8}
tmC <- tmBG + 
  tm_shape(sfC) +
  tm_dots(col = "canal", size = "vol_moy", scale = 2) + 
  tm_scale_bar(text.size = 0.5) +
  tm_layout(legend.outside = TRUE) +
  tm_grid(projection = 4326)

tmC
```

```{r}
ggplot(
    dfC,
    aes(x = annee, y = volume / 1E6)
  ) + 
    geom_col(aes(fill = gsub("^(Canal|Dérivation) (latéral|de|du|des|entre) (à)?\\s*", "", canal)), width = 0.5) +
    scale_x_continuous(breaks = sort(unique(dfC$annee))) +
    labs(x = "Année", y = "Volume (millions m3/an)", fill = "Canal...")
```

```{r}
ggplot(
    dfC %>% group_by(canal, annee) %>% summarise(nbOuv = n()),
    aes(x = annee, y = nbOuv)
  ) + 
    geom_col(aes(fill = gsub("^(Canal|Dérivation) (latéral|de|du|des|entre) (à)?\\s*", "", canal)), width = 0.5) +
    scale_x_continuous(breaks = sort(unique(dfC$annee))) +
    labs(x = "Année", y = "Volume (millions m3/an)", fill = "Canal...")
```

Le volume total annuel et la répartition entre les canaux sont stables entre 2012 et 2018. Par contre le nombre de prises d'eau est multiplié par deux ou trois pour les années 2013-2017 par rapport aux années 2012 et 2018. Il semble que le volume par canal soit correct mais que les volumes ont été agrégés sur une seule prise en 2012 et 2018 et répartis sur toutes les prises des canaux, la plupart du temps uniformément, entre 2013 et 2017.

# Description des canaux et apurement des volumes prélevés



```{r}
viewCanal <- function(sCanal) {
  sf <- sfC %>% filter(canal == sCanal) %>% select(code_ouvrage, nom_ouvrage, nom_commune, vol_min, vol_moy, vol_max) %>% arrange(code_ouvrage)
  df <- dfC %>% filter(canal == sCanal)
  p <- ggplot(
    df,
    aes(x = annee, y = volume / 1E6)
  ) + 
    geom_col(aes(fill = paste0(nom_commune, " (", substring(code_ouvrage, 8), ")")), width = 0.5) +
    scale_x_continuous(breaks = sort(unique(df$annee))) +
    labs(x = "Année", y = "Volume (millions m3/an)", fill = "Commune (ouvrage)")
  return(list(plot = p, sf = knitr::kable(sf)))
}
```

Dans cette partie, nous essayerons de déterminer les volumes prélevés moyens, leur position ainsi que la position des restitutions aux cours d'eau.

```{r}
# Création de l'objet `canals` contenant la description du réseau (voir `?Canals`)
canals <- Canals$new(sfC)
```


## Canal de Bourgogne

> Son point de départ est situé à Migennes ville située sur l'Yonne, un affluent de la Seine ; son point d'arrivée se trouve à Saint-Jean-de-Losne sur la Saône, affluent du Rhône. *(Wikipedia)*

```{r}
v <- viewCanal("Canal de Bourgogne")
v$sf
v$plot
```

> L'alimentation en eau est assurée dans ses parties les plus élevées par 6 réservoirs reliés au canal par des rigoles d'une longueur totale de 63,538 km :
> * les réservoirs de Grosbois (8,6 millions de m3), de Chazilly (2,2 millions de m3 dont le niveau avait été abaissé depuis sa construction) et de Cercey (3,5 millions de m3) près de Pouilly-en-Auxois se déversent dans le bief de partage des eaux ;
> * les réservoirs de Panthier (9 millions de m3), également près de Pouilly-en-Auxois et du Tillot (0,9 million de m3) se déversent sur le versant Saône ;
> * le réservoir de Pont près de Semur-en-Auxois (3 millions de m3) alimente le versant Yonne.
> *(Wikipedia)*

Vérification de la présence de prises sur ce canal plus au Sud du bassin

```{r}
dfSud <- hubeau::get_prelevements_chroniques(list(bbox = "4,46,5,47.5", code_usage = "CAN", annee = "2018"))
knitr::kable(dfSud %>% select(nom_ouvrage, nom_commune, volume))
```

Hormis la prise d'eau de Cercey, les autres points d'alimentation ne sont pas dans la base BNPE mais étant donné le volume de cet ouvrage, il semble qu'il cumule l'ensemble des réservoirs et apports. L'apport se faisant dans le bief de partage, on considère que la restitution à Migennes sur l'Yonne ne récupère que la moitié du volume prélevé.

```{r}
canals$add_bound("Canal de Bourgogne", type = "RIV", "Yonne à Migennes", -0.5, 3.50, 47.96)
```

## Canal de Briare

```{r}
v <- viewCanal("Canal de Briare")
v$sf
v$plot
```

Les données 2012 et 2018 sont contradictoires avec les autres années.
 
Le bief de partage est aussi alimenté par l'usine élévatoire de Briare côté Loire.

```{r}
df <- hubeau::get_prelevements_chroniques(
  list(bbox = "2,47,3,47.7", code_usage = "CAN")
) %>% filter(nom_ouvrage == "CANAL DE BRIARE")
ggplot(df,
       aes(x = annee, y = volume / 1E6, 
           fill = paste0(nom_commune, " (", substring(code_ouvrage, 8), ")"))) + 
  geom_bar(stat = "identity", width = .5, position = "dodge") +
  scale_x_continuous(breaks = sort(unique(df$annee))) +
  labs(x = "Année", y = "Volume (millions m3/an)", fill = "Commune (ouvrage)")
```
L'apport de l'usine élévatoire constuite au moment du passage du canal au gabarit Freyssinet est d'environ 23 millions de m<sup>3</sup>/an est n'est censé être qu'un apport secondaire. De plus, 65 millions de m3/an constitue un débit moyen de 2 m<sup>3</sup>/s se qui semble plus conforme au débit nécessaire pour faire fonctionner les 6 écluses de Rogny-les-Sept-Écluses. On invalide les volumes des années défaillantes.

```{r}
dfC$volume[dfC$code_ouvrage == "OPR0000041115" & dfC$annee %in% seq(2013, 2017)] <- NA
```


Le canal de Briare se jette dans le canal du Loing à Châlette-sur-Loing, l'apport se faisant dans le bief de partage alimenté au trois-quarts par l'ouvrage de Rogny-les-Sept-Écluses avec une restitution de la moitié des volumes côté Loing ($r=0.67$).

```{r}
canals$add_bound("Canal de Briare", type = "CAN", "Canal du Loing", -0.67, 2.7225, 48.028)
```

Il croise le Loing à plusieurs reprises notamment à Rogny-les-Sept-Ecluses où le trop-plein du canal et du Loing sont déversés dans le Loing (Cf. photo ci-dessous).

![Vanne déversante du canal du Loing dans le Loing](v20-Loing_canal_briare.jpg)
 
## Canal de Chelles

```{r}
v <- viewCanal("Canal de Chelles")
v$sf
v$plot
```
 
 > [Le canal de Chelles](https://www.sandre.eaufrance.fr/geo/CoursEau/F66-4002) (ou canal de Vaires à Neuilly-sur-Marne) est un canal navigable parallèle à la Marne en Île-de-France entre Vaires-sur-Marne et Neuilly-sur-Marne de 9,2 km. Il permet de contourner le barrage de Noisiel et évite également des eaux peu profondes au niveau de la réserve des îles protégées de Chelles, Champs-sur-Marne et Gournay-sur-Marne.
> Il traverse les communes de Vaires-sur-Marne au sud, Torcy à sa pointe nord, Chelles au sud, Gournay-sur-Marne au nord-ouest, et Neuilly-sur-Marne au sud-est. 
 
```{r}
canals$add_bound("Canal de Chelles", type = "RIV", "Marne à Neuilly-sur-Marne", -1, 2.5364, 48.854)
```


## Canal de l'Aisne à la Marne

> Il est alimenté par des prises d'eau dans la rivière la Vesle sur son versant Aisne et par un pompage en Marne à Condé-sur-Marne. *(Wikipédia)*

```{r}
v <- viewCanal("Canal de l'Aisne à la Marne")
v$sf
v$plot
```

On retrouve la prise d'eau de la Vesle à Sept-Saulx et la prise à Condé-sur-Marne. 

Il y a une grosse différence entre 2012-2014 et 2015-2018. Il semble que le comptage se soit affiné entre les deux périodes et que 2012-2014 soit invalide.

```{r}
dfC$volume[dfC$code_ouvrage == "OPR0000041105" & dfC$annee %in% c(2012, 2013, 2014)] <- NA
```

Les mêmes valeurs sont saisies pour les deux ouvrages quand les deux sont présents dans la base comme pour la plupart des volumes saisis entre 2013 et 2017 dans la base. D'après @gerardinTheorieMoteursHydrauliques1872 (p.263), l'usine de Condé-sur-Marne fournissait 15 millions de m<sup>3</sup>/an en 1871, et aujourd'hui la capacité de pompage de l'usine de Condé-sur-Marne est de 100 000 m<sup>3</sup>/jour (source: http://www.conde-sur-marne.fr/la-commune/son-patrimoine/l-usine/).

La prise d'eau de Condé-sur-Marne alimenté le versant Marne, le volume est restitué au même endroit. La prise d'eau de Sept-Saulx alimente le versant Aisne qui se déverse dans le canal latéral de l'Aisne à Berry-au-Bac.

```{r}
canals$add_bound("Canal de l'Aisne à la Marne", type = "CAN", "Canal latéral Marne", -0.5, 4.1806, 49.041)
canals$add_bound("Canal de l'Aisne à la Marne", type = "CAN", "Canal latéral à l'Aisne", -0.5, 3.905, 49.399)
```


## Canal de l'Oise à l'Aisne

Fiche Sandre: https://www.sandre.eaufrance.fr/geo/CoursEau/H02-2202

```{r}
v <- viewCanal("Canal de l'Oise à l'Aisne")
v$sf
v$plot
```

```{r}
dfC$volume[dfC$code_ouvrage == "OPR0000041108" & dfC$annee %in% c(2012, 2018)] <- NA
```


Ce canal à bief de partage est restitué dans le canal latéral à l'Oise à Abbécourt et dans le canal latéral de l'Aisne à Bourg-et-Comin.

```{r}
canals$add_bound("Canal de l'Oise à l'Aisne", type = "CAN", "Canal latéral à l'Oise", -0.5, 3.19, 49.592)
canals$add_bound("Canal de l'Oise à l'Aisne", type = "CAN", "Canal latéral à l'Aisne", -0.5, 3.656, 49.388)
```

## Canal de la Haute-Seine

```{r}
v <- viewCanal("Canal de la Haute-Seine")
v$sf
v$plot
```

La prise d'eau de Méry alimente le canal qui se déverse ensuite dans l'Aube à Marcilly-sur-Seine.

```{r}
canals$add_bound("Canal de la Haute-Seine", type = "RIV", "Aube à Marcilly-sur-Seine", -1, 3.718, 48.561)
```

## Canal de la Marne au Rhin

```{r}
v <- viewCanal("Canal de la Marne au Rhin")
v$sf
v$plot
```

Suppression du volume non détaillé en 2012 à la prise d'Etrepy.

```{r}
dfC$volume[dfC$code_ouvrage == "OPR0000041103" & dfC$annee == 2012] <- NA
```

Le canal se rejette dans le Canal latéral Marne à Vitry-le-François.

```{r}
canals$add_bound("Canal de la Marne au Rhin", type = "CAN", "Canal latéral Marne", -1, 4.607, 48.73)
```

## Canal de la Sambre à l'Oise

```{r}
v <- viewCanal("Canal de la Sambre à l'Oise")
v$sf
v$plot
```
Les données 2012 et 2018 pour la prise d'Etreux cumulent les données de Etreux et Hauteville pour les autres années, la répartition est équitable comme pour les autres canaux. La prise d'eau de Lesquielles-Saint-Germain prélève dans l'Oise pour alimenter le canal de la Sambre à l'Oise.

```{r}
dfC$volume[dfC$code_ouvrage == "OPR0000041106" & dfC$annee %in% c(2012, 2018)] <- NA
```

> Le canal de la Sambre à l'Oise est un canal à bief de partage au gabarit Freycinet reliant les vallées de la Sambre et de l'Oise. *(Wikipedia)*

```{r}
dfSambre <- hubeau::get_prelevements_chroniques(list(bbox = "3.5,50,4,51", code_usage = "CAN", annee = "2017"))
knitr::kable(dfSambre %>% select(nom_ouvrage, nom_commune, volume))
```

Le canal de la Sambre à l'Oise rejoint le canal de Saint-Quentin à Tergnier. Les apports dans le bief de partage côté Sambre représentent 10 % du total des apports totaux. 40 % des apports du bassin vont vers la Sambre.

```{r}
canals$add_bound("Canal de la Sambre à l'Oise", type = "CAN", "Canal de Saint-Quentin", -0.6, 3.304, 49.6505)
```

## Canal de Meaux à Chalifert

> Le canal de Meaux à Chalifert ou canal de Chalifert1 est un canal de Seine-et-Marne long de 12,6 kilomètres coupant plusieurs méandres de la Marne, entre Meaux (au niveau du canal Cornillon) et Chalifert. *(Wikipédia)*

Fiche Sandre : https://www.sandre.eaufrance.fr/geo/CoursEau/F6--3102

```{r}
v <- viewCanal("Canal de Meaux à Chalifert")
v$sf
v$plot
```

```{r}
canals$add_bound("Canal de Meaux à Chalifert", type = "RIV", "Marne à Chalifert", -1, 2.77, 48.898)
```

## Canal de Saint Quentin

```{r}
v <- viewCanal("Canal de Saint Quentin")
v$sf
v$plot
```

Comme pour les autres canaux, il doit exister une seconde prise en dehors du bassin versant et le volume est réparti équitablement entre les deux prises entre 2013 et 2014. A défaut, on garde cette répartition 50/50 pour définir le volume sur la prise de Vadencourt.

```{r}
dfC$volume[dfC$code_ouvrage == "OPR0000041107" & dfC$annee %in% c(2012, 2018)] <- NA
```


Le canal se déverse dans le canal latéral à l'Oise à Chauny.

```{r}
canals$add_bound("Canal de Saint Quentin", type = "CAN", "Canal latéral à l'Oise", -1, 3.2248, 49.6078)
```

## Canal des Ardennes

> Long de 87,779 kilomètres, il comporte 44 écluses (37 sur le versant Aisne et 7 sur le versant Meuse), et un tunnel à Saint-Aignan.
> Il relie le village de Pont-à-Bar, sur la commune de Dom-le-Mesnil (Ardennes) à la commune de Vieux-lès-Asfeld (Ardennes). Cette première partie du canal longue de 39 km permet de franchir le seuil entre les vallées de la Meuse et de l'Aisne en empruntant la vallée de la Bar, délaissée brièvement à Saint-Aignan pour un raccourci à travers un tunnel. Sur cette partie à bief de partage, il est alimenté par une réserve d'eau (lac de Bairon) et par un pompage en Meuse. Après le bief de partage il descend rapidement vers l'Aisne par une suite de 26 écluses sur seulement 9 km. À partir de Semuy le canal suit de près le cours de l'Aisne. Par endroits il emprunte même l'ancien lit de la rivière et présente de nombreux virages alors que la rivière a été déviée dans des tronçons très rectilignes. Sur son cours latéral à l'Aisne, il est alimenté par des prises d'eau régulières dans la rivière (barrages de Vouziers, Rilly, Givry, Biermes et Asfeld). *(Wikipédia)* 

```{r}
v <- viewCanal("Canal des Ardennes")
v$sf
v$plot
```

```{r}
dfC$volume[dfC$code_ouvrage == "OPR0000041109" & dfC$annee %in% c(2012, 2018)] <- NA
```


Le canal se rejette dans le canal latéral à l'Aisne.

```{r}
canals$add_bound("Canal des Ardennes", type = "CAN", "Canal latéral à l'Aisne", -1, 4.095, 49.4482)
```


## Canal du Loing

```{r}
v <- viewCanal("Canal du Loing")
v$sf
v$plot
```

```{r}
dfC$volume[dfC$code_ouvrage == "OPR0000041116" & dfC$annee %in% c(2012, 2018)] <- NA
```

Le canal reçoit les eaux du canal de Briare à Corquilleroy et se rejette dans le Loing à un kilomètre de la confluence avec la Seine à Moret-sur-Loing.

```{r}
canals$add_bound("Canal du Loing", type = "RIV", "Loing à Moret-sur-Loing", -1, 2.818, 48.378)
```

              
## Canal du Nivernais

```{r, fig.height=5}
v <- viewCanal("Canal du Nivernais")
v$sf
v$plot
```

> L'alimentation est effectuée via la rigole d'Yonne qui achemine l'eau du lac de Pannecière jusqu'au bief de partage (point culminant du canal) délimité par les écluses de Baye et de Port Brûlé. Elle est aussi assurée par les étangs de Vaux et Baye.
> À partir du bief de partage, l'eau s'écoule jusqu'à Châtillon-en-Bazois pour le versant Loire, ensuite c'est la rivière Aron qui apporte son eau jusqu'à Saint-Léger-des-Vignes.
> Pour le versant Yonne, la rivière Yonne alimente en eau le canal du Nivernais à partir de La Chaise (commune de Corbigny) et la Cure se joint à elle via l'embranchement de Vermenton jusqu'à Auxerre. Entre Clamecy et Auxerre, le canal fait souvent lit commun avec l'Yonne (des « râcles »), ce qui contribue à alimenter naturellement le canal. *(Wikipedia)* 

Les prises d'eau sur L'Yonne entre Clamecy et Auxerre concernent des dérivations de l'Yonne là où le canal et l'Yonne font lit commun. Ces prises ne sont pas en prendre en compte.

```{r}
dfC$volume[dfC$code_ouvrage == "OPR0000041117" & dfC$annee %in% c(2012, 2018)] <- NA

prises <- dfC$canal == "Canal du Nivernais" & 
  dfC$longitude > 3.5 & dfC$latitude > 47.45 & 
  dfC$longitude < 3.8 & dfC$latitude < 47.8 
dfC$volume[prises] <- NA
```

La règle de gestion de la rigole Yonne est la suivante [@dehayEtudeImpactChangement2012] (Débits moyens mensuels en m<sup>3</sup>/s) :

```{r}
debitRigole <- t(as.matrix(c(0, 0, 0.6, 1.1, 1.2, 1.3, 1.4, 1.4, 1.3, 1.1, 0.6, 0)))
colnames(debitRigole) <- paste(seq_len(12))
knitr::kable(debitRigole)
```
Le total annuel moyen prélevé par la rigole Yonne pour le canal du Nivernais devrait être de `r mean(debitRigole) * 86400 * 365 / 1E6` millions de m<sup>3</sup>/an.

```{r}
dfC$volume[dfC$code_ouvrage == "OPR0000041117" & dfC$annee %in% seq(2013, 2017)] <- mean(debitRigole) * 86400 * 365
```

```{r}
v <- viewCanal("Canal du Nivernais")
v$plot
```

Le canal se déverse dans l'Yonne à Clamecy à l'endroit où il commence à faire lit commun avec l'Yonne :

```{r}
canals$add_bound("Canal du Nivernais", type = "RIV", "Yonne à Clamecy", -1, 3.523, 47.4585)
```



## Canal du Nord

```{r}
v <- viewCanal("Canal du Nord")
v$sf
v$plot
```
Le Canal du Nord est alimenté par le canal latéral l'Oise à Pont-l'Evêque et se rejette dans le bassin versant de la Somme.

```{r}
dfC$interne[dfC$code_ouvrage == "OPR0000041110"] <- TRUE
dfC$volume[dfC$code_ouvrage == "OPR0000041110" & dfC$annee %in% c(2012, 2013)] <- NA
```

## Canal entre Champagne et Bourgogne

```{r}
v <- viewCanal("Canal entre Champagne et Bourgogne")
v$sf
v$plot
```

```{r}
dfC$volume[dfC$canal == "Canal entre Champagne et Bourgogne" & dfC$annee %in% c(2012)] <- NA
```



Le canal se jette dans le canal latéral à la Marne à Vitry-le-François.

```{r}
canals$add_bound("Canal entre Champagne et Bourgogne", type = "CAN", "Canal latéral Marne", -1, 4.607, 48.73)
```

## Canal latéral à l'Aisne

> Long de 52,5 kilomètres (...). Il relie les villes de Vieux-lès-Asfeld et de Celles-sur-Aisne en passant par Berry-au-Bac. *(Wikipedia)*

Il est alimenté par le canal des Ardennes à Vieux-lès-Asfeld et se rejette dans l'Aisne à Celles-sur-Aisne.

```{r}
v <- viewCanal("Canal latéral à l'Aisne")
v$sf
v$plot
```

```{r}
canals$add_bound("Canal latéral à l'Aisne", type = "RIV", "Aisne à Celles-sur-Aisne", -1, 3.481, 49.4)
```

## Canal latéral à l'Oise

> Le canal latéral à l’Oise est un canal de gabarit Freycinet qui dessert l’Est de la Picardie. D'une longueur de 34 km, il connecte le canal de Saint-Quentin (depuis Chauny) à l'Oise canalisée à hauteur de Janville. *(Wikipédia)*

```{r}
v <- viewCanal("Canal latéral à l'Oise")
v$sf
v$plot
```

```{r}
dfC$interne[dfC$code_ouvrage == "OPR0000041112"] <- TRUE
```

```{r}
canals$add_bound("Canal latéral à l'Oise", type = "RIV", "Oise à Janville", -1, 2.865, 49.455)
```

## Canal latéral Marne

Le canal reçoit les eaux du canal de la Marne au Rhin et du canal entre Champagne et Bourgogne à Vitry-le-François. Il est aussi alimenté par des prises sur la Marne (cf. ouvrages ci-dessous).

```{r}
v <- viewCanal("Canal latéral Marne")
v$sf
v$plot
```

A priori les prises d'eau de Couvrot correspondent au volume apporté par les canaux de la Marne au Rhin et entre Champagne et Bourgogne à Vitry-le-François.

```{r}
dfC$interne[dfC$nom_commune == "Couvrot"] <- TRUE
```

```{r}
canals$add_bound("Canal latéral Marne", type = "RIV", "Marne à Hautvillers", -1, 3.9437, 49.0705)
```


## Dérivation de Beaulieu à Villiers

> Le canal de Beaulieu ou canal de dérivation de Beaulieu à Villiers-sur-Seine est un canal navigable, long de 9,1 kilomètres, évitant les nombreux méandres de la Seine, au nord du fleuve, entre Le Mériot et Noyen-sur-Seine. *(Wikipedia)*

```{r}
v <- viewCanal("Dérivation de Beaulieu à Villiers")
v$sf
v$plot
```

```{r}
canals$add_bound("Dérivation de Beaulieu à Villiers", type = "RIV", "Seine à Noyen-sur-Seine", -1, 3.359, 3.359)
```

## Dérivation de Conflans à Bernières

Il est alimenté par la Seine à Conflans-sur-Seine et se rejette dans la Seine à Nogent-sur-Seine. Il court-circuite la station hydrométrique de de Pont-sur-Seine.

```{r}
v <- viewCanal("Dérivation de Conflans à Bernières")
v$sf
v$plot
```

```{r}
canals$add_bound("Dérivation de Conflans à Bernières", type = "RIV", "Seine à Nogent-sur-Seine", -1, 3.526, 48.51)
```

## Dérivation de Courlon

Il dérive l'Yonne à Courlon-sur-Yonne et rejoint l'Yonne à Misy-sur-Yonne.

```{r}
v <- viewCanal("Dérivation de Courlon")
v$sf
v$plot
```

```{r}
canals$add_bound("Dérivation de Courlon", type = "RIV", "Yonne à Misy-sur-Yonne", -1, 3.103, 48.3495)
```

## Dérivation de Gurgy

```{r}
v <- viewCanal("Dérivation de Gurgy")
v$sf
v$plot
```

```{r}
canals$add_bound("Dérivation de Gurgy", type = "RIV", "Yonne à Gurgy", -1, 3.533, 47.9095)
```

## Dérivation de Joigny

```{r}
v <- viewCanal("Dérivation de Joigny")
v$sf
v$plot
```

```{r}
canals$add_bound("Dérivation de Joigny", type = "RIV", "Yonne à Saint-Aubin-sur-Yonne", -1, 3.342, 48.)
```

# Bilan des volumes moyens annuels aux prises et restitutions

Les volumes aux restitutions sont calculées à partir de la somme des prises de chaque canal en appliquant le ratio définit plus haut pour chaque restitution.

```{r}
sfCanals <- canals$get_all(dfC) %>%
  mutate(type_complet = ifelse(
    ratio > 0, 
    ifelse(interne, "prise en canal", "prise en rivière"), 
    ifelse(type == "RIV", "restitution en rivière", "restitution en canal")), 
    volume_abs = abs(volume))
tmCanals <- tmBG + 
  tm_shape(sfCanals) +
  tm_dots(col = "canal.name", size = "volume_abs", shape = "type_complet", scale = 2, shapes = c(21, 22, 24, 25)) + 
  tm_scale_bar(text.size = 0.5) +
  tm_layout(legend.outside = TRUE) +
  tm_grid(projection = 4326)

tmCanals
```

