
<!-- README.md is generated from README.Rmd. Please edit that file -->

# InWopUsages

<!-- badges: start -->

<!-- badges: end -->

Ce package regroupe les données et analyses de la modélisation des
usages sur le bassin versant de la Seine dans le cadre du projet IN-WOP.

## Installation

We need the package `remotes` to install the package from the Irstea
Gitlab repository:

``` r
install.packages("remotes")
```

The package **InWopUsages** is under development and is only available
on Gitlab:

``` r
remotes::install_gitlab("in-wop/inwopusages", host = "gitlab.irstea.fr", dependencies = TRUE, build_vignettes = TRUE)
```

`dependencies = TRUE` and `build_vignettes = TRUE` are optional and
respectively trigger the installation of suggested packages used in the
vignettes and the compilation and the installation of the vignettes.

## Exploration du package

La documentation des variables et les rapports sont visibles sur le site
internet du package: <https://in-wop.g-eau.fr/usages>

## Constitution de la base de données

Ce package contient des données synthétiques qui seront utilisées dans
les modèles d’usages.

Elles sont constituées à partir d’une base de données stockées à
l’adresse suivante :
<https://owncloud.dorch.fr/index.php/s/Rk6PwTlKvyjRTSe?path=%2F2_Data%2Fusages>

Les données synthétiques sont les suivantes:

  - `territoire`: données géographiques des limites administratives du
    BV
  - `bnpe`: données extraites de la BNPE sur le BV
  - `aep`: données de l’INSEE utilisée pour la régression des
    prélèvement AEP

Chacune de ces données est accessible avec la commande `data`. Exemple:

``` r
library(InWopUsages)
#> Loading required package: tidyverse
#> Warning: package 'tidyverse' was built under R version 4.0.5
#> -- Attaching packages --------------------------------------- tidyverse 1.3.1 --
#> v ggplot2 3.3.3     v purrr   0.3.4
#> v tibble  3.1.1     v dplyr   1.0.5
#> v tidyr   1.1.3     v stringr 1.4.0
#> v readr   1.4.0     v forcats 0.5.1
#> Warning: package 'tibble' was built under R version 4.0.5
#> -- Conflicts ------------------------------------------ tidyverse_conflicts() --
#> x dplyr::filter() masks stats::filter()
#> x dplyr::lag()    masks stats::lag()
data(territoire)
str(territoire)
#> List of 2
#>  $ com: tibble[,5] [5,610 x 5] (S3: tbl_df/tbl/data.frame)
#>   ..$ insee_com: chr [1:5610] "02001" "02002" "02003" "02004" ...
#>   ..$ lib_com  : chr [1:5610] "Abbécourt" "Achery" "Acy" "Agnicourt-et-Séchelles" ...
#>   ..$ id_dep   : chr [1:5610] "02" "02" "02" "02" ...
#>   ..$ id_arr   : chr [1:5610] "022" "022" "024" "022" ...
#>   ..$ surf_ha  : num [1:5610] 599 698 1162 1086 1068 ...
#>  $ arr: tibble[,3] [74 x 3] (S3: tbl_df/tbl/data.frame)
#>   ..$ id_arr    : chr [1:74] "021" "022" "023" "024" ...
#>   ..$ lib_arr   : chr [1:74] "Château-Thierry" "Laon" "Saint-Quentin" "Soissons" ...
#>   ..$ nb_com_arr: num [1:74] 118 278 126 153 130 160 101 120 104 80 ...
```

Et une documentation de la variable est accessible avec la commande `?`.
Exemple : `?territoire`
